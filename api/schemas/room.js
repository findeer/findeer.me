var Room = {
    identifier: {type: String, required: "{PATH} is required"},
    name: {type: String, required: "{PATH} is required"},
    floor: {type: Number, required: "{PATH} is required"},
    calendarId: {type: String, default: ""},
    image: {type: String, default: ""},
    description: {type: String, default: ""},
    features: {
        vc: {type: Boolean, required: "{PATH} is required"},
        vcIp: {type: String, default: ""},
        capacity: {type: Number, required: "{PATH} is required"},
        projector: {type: Boolean, required: "{PATH} is required"},
    },
    createdAt: {type: Date, default: Date.now},
    modifiedAt: {type: Date, default: Date.now}
};

module.exports = Room;
