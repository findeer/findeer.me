module.exports = {
    Room: require('./room.js'),
    Person: require('./person.js'),
    Sensor: require('./sensor.js')
};
