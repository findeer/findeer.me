var Sensor = {
    identifier: {type: String, required: "{PATH} is required"},
    room: {type: String, required: "{PATH} is required"},
    status: {type: Boolean, required: "{PATH} is required"},
    createdAt: {type: Date, default: Date.now},
    modifiedAt: {type: Date, default: Date.now}
};

module.exports = Sensor;
