var Person = {
    firstName: {type: String, required: "{PATH} is required"},
    lastName: {type: String, required: "{PATH} is required"},
    floor: {type: Number, required: "{PATH} is required"},
    team: {type: String, required: "{PATH} is required"},
    role: {type: String, required: "{PATH} is required"},
    competences: [String],
    createdAt: {type: Date, default: Date.now},
    modifiedAt: {type: Date, default: Date.now}
};

module.exports = Person;
