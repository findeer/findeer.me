var mongoose = require('mongoose'),
    mongooseTypes = require('mongoose-types');

mongooseTypes.loadTypes(mongoose);

module.exports = {
    Email: mongoose.SchemaTypes.Email,
    Url: mongoose.SchemaTypes.Url
};
