var restify = require('restify'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    mongooseAI = require('mongoose-auto-increment'),
    mongooseTypes = require('mongoose-types'),
    Schemas = require('./schemas/'),
    Errors = require('./errors/'),
    Server,
    Models = {},
    DEFAULTS = {
        limit: 100
    },
    sockjs = require('sockjs');

Server = function(config) {
    this.initialize(config || {});
};

Server.prototype = {
    initialize: function(config) {
        var db = mongoose.connection;

        db.on('error', console.error.bind(console, 'Database connection error:'));
        db.once('open', function() {
            mongooseTypes.loadTypes(mongoose);
            mongooseAI.initialize(db.base.connections[0]);
            this.initializeSchemas();
            this.initializeRoutes();
        }.bind(this));

        mongoose.connect('mongodb://heroku_app35961291:li70r13jrufvd89g92s5dm2q3t@ds037601.mongolab.com:37601/heroku_app35961291')
    },
    initializeSchemas: function() {
        _.each(Schemas, function(schema, name) {
            var compiledSchema = mongoose.Schema(schema);
            compiledSchema.plugin(mongooseAI.plugin, name);
            Models[name] = mongoose.model(name, compiledSchema);
        });
    },
    initializeRoutes: function() {
        this.server = restify.createServer({
            name: 'FindeerAPI'
        });

        this.server.use(restify.gzipResponse());
        this.server.use(restify.bodyParser({ mapParams: true }));
        this.server.use(restify.queryParser());
        //this.server.use(restify.CORS());

        this.server.get('/rooms', this.crud.list.bind(this, 'Room'));
        this.server.post('/rooms', this.crud.create.bind(this, 'Room'));
        this.server.get('/rooms/:id', this.readDocumentById.bind(this, 'Room'));
        this.server.put('/rooms/:id', this.updateDocument.bind(this, 'Room'));
        this.server.del('/rooms/:id', this.deleteDocument.bind(this, 'Room'));

        this.server.get('/sensors', this.crud.list.bind(this, 'Sensor'));
        this.server.post('/sensors', this.crud.create.bind(this, 'Sensor'));
        this.server.get('/sensors/:id', this.readDocumentById.bind(this, 'Sensor'));
        this.server.put('/sensors/:id', this.updateDocument.bind(this, 'Sensor'));
        this.server.del('/sensors/:id', this.deleteDocument.bind(this, 'Sensor'));

        this.server.get('/people', this.crud.list.bind(this, 'Person'));
        this.server.post('/people', this.crud.create.bind(this, 'Person'));
        this.server.get('/people/:id', this.readDocumentById.bind(this, 'Person'));
        this.server.put('/people/:id', this.updateDocument.bind(this, 'Person'));
        this.server.del('/people/:id', this.deleteDocument.bind(this, 'Person'));

        var socketServer = sockjs.createServer();

        var self = this;

        socketServer.on('connection', function(conn) {
            conn.on('data', function(message) {
                console.log('connection on data', message);
                conn.write(message);
            });

            self.readDocuments.bind(self, 'Room', null, null, null, null, function(err, data) {
                _(data).forEach(function(n) {
                    conn.write(JSON.stringify({
                        'id': n.identifier,
                        'status': 0
                    }));
                });
            })();

            conn.on('close', function() {
                console.log('connection closed');
            })
        });

        socketServer.installHandlers(this.server.server, { prefix: '/status' });

        // if no route is matched serve static files
        this.server.get(/.*/, restify.serveStatic({
            directory: './app/public/',
            default: 'index.html'
        }));

        this.server.listen(process.env.PORT || 5000, function() {
            console.log('%s listening at %s', this.server.name, this.server.url);
        }.bind(this));
    },
    parseFormParams: function(params) {
        var parsed = {};

        // parse array fields
        _.each(params, function(value, key) {
            if(key.match(/[^\[]\[[0-9]*\]/)) {
                key = key.substr(0, key.indexOf('['));
                if(typeof parsed[key] === 'undefined') {
                    parsed[key] = [];
                }
                parsed[key].push(value);
            } else {
                parsed[key] = value;
            }
        });

        return parsed;
    },
    validateId: function(id, res, next) {
        id = parseInt(id);

        if(_.isNaN(id)) {
            res.send(new restify.NotFoundError());
            next();
            return false;
        } else {
            return id;
        }
    },
    sendData: function(req, res, next, err, data) {
        if(err) {
            res.send(err);
        } else {
            if(data === null) {
                res.send(new restify.NotFoundError());
            } else {
                res.send(data);
            }
        }
        return next();
    },
    updateDocument: function(model, req, res, next) {
        var id = this.validateId(req.params.id, res, next),
            changes = {}, params = JSON.parse(req.body);


        if(id === false) {
            return;
        }

        //filter out only valid changed fields
        _.each(_.omit(params, ['_id', 'id']), function(value, key) {
            if(!_.isUndefined(Models[model].schema.path(key))) {
                changes[key] = value;
            }
        });

        if(_.isEmpty(changes)) {
            res.send(204);
            next();
        } else {
            changes.modifiedAt = Date.now();
            Models[model].update({_id: id}, {$set: changes}, function(err) {
                if(err) {
                    if(err.errors) {
                        res.send(new Errors.ValidationError(err.message, err.errors));
                    } else {
                        res.send(new restify.ServiceUnavailableError());
                    }
                } else {
                    res.send(204);
                }
                next();
            });
        }
    },
    createDocument: function(model, body, cb) {
        Models[model].create(body, cb);
    },
    deleteDocument: function(model, req, res, next) {
        var id = this.validateId(req.params.id, res, next);

        if(id === false) {
            return;
        }

        Models[model].findById(id, function(err, data) {
            if(err) {
                res.send(err);
                return next();
            } else {
                if(!data) {
                    res.send(new restify.NotFoundError());
                } else {
                    data.remove(function(err, data) {
                        if(err) {
                            res.send(err)
                        } else {
                            res.send(200);
                        }
                        return next();
                    });
                }
            }
        });
    },
    readDocumentById: function(model, req, res, next) {
        var id = this.validateId(req.params.id, res, next);

        if(id === false) {
            return;
        }

        Models[model].findById(id, this.sendData.bind(this, req, res, next));
    },
    // Ref: http://mongoosejs.com/docs/populate.html
    readDocumentByIdAndPopulate: function(model, populate, req, res, next) {
        if(arguments.length !== 5 || !populate.path) {
            throw new Error('Missing populate path name');
        }

        populate.select = populate.select || '';

        if(req.params.with) {
            var params = _.omit(req.params.with.split('|'), '_id');
            _.each(params, function(value) {
                populate.select += ' ' + value;
            });
        }

        var id = this.validateId(req.params.id, res, next);

        if(id === false) {
            return;
        }

        Models[model].findById(id).populate(populate.path, populate.select || '').exec(this.sendData.bind(this, req, res, next));
    },
    readDocuments: function(model, query, fields, skip, limit, cb) {
        query = query || {};
        fields = fields || '';
        skip = skip || 0;
        limit = limit || DEFAULTS['limit'];
        Models[model].find(query, fields, {
            skip: skip, limit: limit
        }, cb);
    },
    crud: {
        list: function(model, req, res, next) {
            var limit = req.params.limit,
                page = req.params.page || 0,
                skip = page * limit,
                query = {},
                filters;

            // parse filters param in form of filters=key::value|key2::value2|...
            if(req.params.filter) {
                filters = req.params.filter.split('|');

                _.each(filters, function(filter) {
                    filter = filter.split('::');
                    query[filter[0]] = filter[1];
                });
            }

            this.readDocuments(model, query, null, skip, limit, this.sendData.bind(this, req, res, next));
        },
        create: function(model, req, res, next) {
            this.createDocument(model, JSON.parse(req.body), function(err, model) {
                if(err) {
                    res.send(new Errors.ValidationError(err.message, err.errors));
                } else {
                    res.send(model);
                }
                return next();
            });
        }
    }
};

// TODO: initialize in a separate file with config & move generic stuff to a separate npm module
new Server();
