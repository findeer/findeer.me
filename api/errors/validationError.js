var restify = require('restify');
var util = require('util');
var _ = require('lodash');

function ValidationError(message, errors) {
    var parsedErrors = {};

    // custom handling for unique index validation error
    if(message.indexOf('E11000') === 0) {
        var path = message.match(/\$([^_]*)_/),
            value = message.match(/"([^"]*)"/);

        if(_.isArray(path) && _.isArray(value) && path[1] && value[1]) {
            path = path[1];
            value = value[1];

            message = "Validation error";
            errors = {};
            errors[path] = {
                "message": path + " must be unique",
                "name": "ValidatorError",
                "kind": "user defined",
                "path": path,
                "value": value
            };
        }
    }

    _.each(errors, function(value, key) {
        parsedErrors[key] = _.omit(value, 'properties');
    });

    restify.RestError.call(this, {
        restCode: 'ValidationError',
        statusCode: 400,
        constructorOpt: ValidationError,
        body: {
            message: message,
            code: 'ValidationError',
            errors: parsedErrors
        }
    });
    this.name = 'ValidationError';
}

util.inherits(ValidationError, restify.RestError);

module.exports = ValidationError;
