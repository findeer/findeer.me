var entry = './app/src/js/main.js',
    distPath  = __dirname + '/app/public',
    output = {
        path: distPath,
        filename: 'main.js'
    };

module.exports.development = {
    debug: true,
    devtool: 'eval',
    entry: entry,
    output: output,
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: [/node_modules/, /api/],
                loader: 'babel-loader'
            },
            {
                test: /\.html$/,
                exclude: [/node_modules/, /api/],
                loader: "ejs-loader"
            }
        ]
    }
};

module.exports.production = {
    debug: false,
    entry: entry,
    output: output,
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: [/node_modules/, /api/],
                loader: 'babel-loader'
            },
            {
                test: /\.html$/,
                exclude: [/node_modules/, /api/],
                loader: "ejs-loader"
            }
        ]
    }
};
