import HelloView from './views/hello';
import DashboardView from './dashboard/index';

export default Backbone.Router.extend({
    routes: {
        '': 'dashboard',
        '!/room/:id': 'room',
        '!/floor/:id': 'floor',
        'about': 'about'
    },

    initialize() {},

    start() {
        Backbone.history.start();
    },

    dashboard() {
        this.trigger('viewChange', DashboardView);
    },

    room(id) {
        this.trigger('viewChange', DashboardView, {
            roomId: id
        });
    },

    floor(id) {
        this.trigger('viewChange', DashboardView, {
            floorId: id
        });
    },

    about() {
        var helloView = new HelloView({
            template: _.template('Im the about page')
        }).render();
    }
});
