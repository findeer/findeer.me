import SearchView from './../search/index';
import MapView from './../map/index';
import RoomView from './../room/index';

import template from './index.html';

export default Backbone.View.extend({
    options: {},

    initialize(options) {
        this.options = options;
    },

    render() {
        this.$el.html(template({
            name: 'test'
        }));

        this.searchView = new SearchView({
            el: this.$(".view-search")
        });
        this.searchView.render();

        this.mapView = new MapView({
            el: this.$(".view-map"),
            id: this.options.floorId
        });
        this.mapView.render();

        if(this.options.roomId) {
            this.renderRoomDetails(this.options.roomId);
        }

        return this;
    },

    renderRoomDetails(id) {
        this.roomView = new RoomView({
            el: this.$(".view-details"),
            id: id
        });
        this.roomView.render();
    }
});
