import template from './index.html';

export default Backbone.View.extend({
    id: null,

    initialize(options) {
        this.id = options.id || null;
    },

    render() {
        this.$el.html(template({
            id: this.id
        }));

        return this;
    }
});
