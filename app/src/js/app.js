let Application = {
    initialize(router) {
        this.$container = $('#content');

        this.router = router;
        this.router.on('viewChange', (view, options) => {
            this.setCurrentView(view, options);
        });

        this.router.start();
    },

    setCurrentView(View, options) {
        options = options || {};

        if(this.currentView) {
            this.currentView.undelegateEvents();
            this.currentView.stopListening();

            if(this.currentView.destroy) {
                this.currentView.destroy();
            }
        }

        this.currentView = new View(_.extend({
            el: this.$container
        }, options));

        this.currentView.render();
    }
};

let instance;

if(!instance) {
    instance = _.extend(Backbone.Events, Application);
}

export default instance;
