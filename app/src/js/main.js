import Application from './app';
import Router from './router';

Application.initialize(new Router());
