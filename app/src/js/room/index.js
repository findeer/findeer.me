import RoomModel from './model';
import template from './index.html';

export default Backbone.View.extend({
    model: null,

    initialize(options) {
        this.model = new RoomModel({
            id: options.id
        });
    },

    destroy: function() {
        this.$el.removeClass('active')
    },

    render() {
        let request = this.model.fetch();
        request.done(this.onFetchSuccess.bind(this));
        request.fail(this.onFetchError.bind(this));

        return this;
    },

    renderDetails: function() {
        this.$el.html(template({
            item: this.model
        })).addClass('active');
    },

    onFetchSuccess() {
        this.renderDetails();
    },

    onFetchError: () => {
        console.log("API ERRPR");
    }
});
