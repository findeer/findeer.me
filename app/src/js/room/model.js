export default Backbone.Model.extend({
    defaults: {
        visible: true
    },

    url() {
        return 'https://findeer.herokuapp.com/rooms/' + this.getId();
    },

    parse(data) {
        data.id = data._id;
        return _.omit(data, ['_id']);
    },

    getId() {
        return this.get("id");
    },

    // dom id
    getIdentifier() {
        return this.get("identifier");
    },

    getDisplayName() {
        return this.get("name");
    },

    isVisible() {
        return this.get("visible") === true;
    },

    matchQuery(query) {
        let displayName = this.getDisplayName().toLowerCase();

        // no query everything matches
        if(query === "") {
            return true;
        }

        return displayName.indexOf(query) > -1;
    }
});
