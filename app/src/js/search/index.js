import SearchCollection from './collection';

import indexTemplate from './index.html';
import listTemplate from './list.html';

export default Backbone.View.extend({
    events: {
        "keyup .search-query": "onSearchChange"
    },

    initialize() {
        this.collection = new SearchCollection();
    },

    render() {
        this.$el.html(indexTemplate());
        this.$list = this.$(".view-search-results");

        this.collection.fetch()
            .done(this.onFetchSuccess.bind(this))
            .fail(this.onFetchError.bind(this));

        return this;
    },

    renderList(items) {
        this.$list.html(listTemplate({
            items: items
        }));
    },

    filterList(items) {
        _.each(items, (item) => {
            var $item = this.$list.find("li[data-id='" + item.getId() + "']");

            if(item.isVisible()) {
                $item.addClass("item-visible");
            } else {
                $item.removeClass("item-visible");
            }
        });
    },

    onSearchChange(event) {
        let $target = $(event.currentTarget);
        this.filterList(this.collection.search($target.val()));
    },

    onFetchSuccess() {
        this.renderList(this.collection);
    },

    onFetchError: () => {
        console.log("API ERRPR");
    }
});
