import SearchModel from './../room/model'

export default Backbone.Collection.extend({
    model: SearchModel,

    url() {
        return 'https://findeer.herokuapp.com/rooms';
    },

    search: function(string) {
        return this.map((model) => {
            // if query not match then hide
            model.set("visible", model.matchQuery(string));

            return model;
        });
    }
});
