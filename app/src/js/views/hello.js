import template from './hello.html';

export default Backbone.View.extend({
    serializeData() {
        return {
            name: 'world test'
        };
    },

    render() {
        this.$el.html(template({
            name: 'test'
        }));

        return this;
    }
});
